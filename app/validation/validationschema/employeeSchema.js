const Joi= require('joi')
const employeeSchema ={
postEmployeeSchema : Joi.object().keys({ 
  employeeRollNumber: Joi.string().min(3).max(5).required(),
  employeeName: Joi.string().min(3).max(30).required(), 
  employeeAge: Joi.number().min(18).max(100).required() ,
  employeeMobileNumber:Joi.string().max(12).required() ,
  employeeEmail:Joi.string().min(3).max(30).required(),
  employeeAddress:Joi.array().items(Joi.object().keys({
    street:Joi.string().min(3).max(30).required(),
    city:Joi.string().min(3).max(30).required(),
    state:Joi.string().min(3).max(30).required(),
    zip:Joi.string().min(3).max(10).required()
  }))

  }),

  updateEmployeeSchema :  Joi.object().keys({ 
    _id:Joi.string().pattern(new RegExp("^[0-9a-fA-F]{24}$")).min(24).required(),
    employeeRollNumber: Joi.string().min(3).max(5).required(),
    employeeName: Joi.string().min(3).max(30).required(), 
    employeeAge: Joi.number().min(18).max(100).required() ,
    employeeMobileNumber:Joi.string().max(12).required() ,
    employeeEmail:Joi.string().min(3).max(30).required(),
    employeeAddress:Joi.array().items(Joi.object().keys({
      _id:Joi.string().pattern(new RegExp("^[0-9a-fA-F]{24}$")).min(24).required(),
      street:Joi.string().min(3).max(30).required(),
      city:Joi.string().min(3).max(30).required(),
      state:Joi.string().min(3).max(30).required(),
      zip:Joi.string().min(3).max(10).required()
    }))
  
    }),

  deleteEmployeeSchema:Joi.object().keys({
      id:Joi.string().pattern(new RegExp("^[0-9a-fA-F]{24}$")).min(24).required(),
  })
}
module.exports=employeeSchema