const Joi = require('joi');
const validationMiddleware = (schema, property) => {
    return (req, res, next) => {
        try {
    console.log(req[property])
            const { value, error } = schema.validate(req[property]);
            if (value && Object.keys(value).length && !error) {
                next();
            } else {
                const { details } = error;
                const message = details.map(i => i.message).join(',');
                res.status(422).json({ error: message })
            }

        } catch (e) {
            console.log("ddddddddddddd", e)
        }
    }
}
module.exports = validationMiddleware;