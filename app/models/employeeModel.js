const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
  Schema = mongoose.Schema;
const EmployeeSchema = new mongoose.Schema(
  {
    employeeRollNumber: String,
    employeeName:String,
    employeeAge:Number,
    employeeMobileNumber:String,
    employeeEmail:String,
    employeeAddress:[{
      street:String,
    city:String,
    state:String,
    zip:String
}]
  }

,{collection:"employee",  versionKey: false,
});
const employee= mongoose.model('employee',EmployeeSchema);

  module.exports = employee