const express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    employeeModel = mongoose.model('employee'),
    employeeSchema = require("../validation/validationschema/employeeSchema"),
    validationMiddleware = require("../validation/validationMiddleware/validate");
mongoose.Promise = global.Promise,


    module.exports = function (app) {
        app.use('/', router);
    };

router.get("/", (req, res) => {
    res.send("Welcome to Employee Management")
})

router.post('/employeeDetails', validationMiddleware(employeeSchema.postEmployeeSchema, "body"), async (req, res) => {
    try {
        const duplicateEmployee = await employeeModel.find({ "employeeEmail": req.body.employeeEmail })
        if (duplicateEmployee && duplicateEmployee.length) {
            res.status(400).send(new Error('Employee already exist'));
        } else {
            const newemployeeModel = await new employeeModel(req.body);
            const savedEmployee = await newemployeeModel.save()
            res.send(savedEmployee)

        }
    } catch (e) {
        console.log("eee", e)
        res.status(500).send(new Error('Please try again later'));
    }


})


router.get('/employeeDetails', async (req, res) => {
    try {
        const savedEmployee = await employeeModel.find({})
        res.send(savedEmployee)

        }
     catch (e) {
        res.status(500).send(new Error('Please try again later'));

    }


})




router.put('/employeeDetails', validationMiddleware(employeeSchema.updateEmployeeSchema, "body"), async (req, res) => {
    try {
        const findDuplicateEmployee = await employeeModel.findOne({ "_id": { $ne: req.body._id }, employeeEmail: req.body.employeeEmail })
        if (findDuplicateEmployee && Object.keys(findDuplicateEmployee).length) {
            res.status(400).send(new Error('Employee Already exist'));

        } else {

            const updatedEmployee = await employeeModel.findOneAndUpdate({ "_id": req.body._id }, { $set: req.body }, { returnNewDocument: true, upsert: false, new: true })
            if (updatedEmployee && Object.keys(updatedEmployee).length)
                res.send(updatedEmployee)
            else
            res.status(400).send(new Error('Employee not available'));


        }
    } catch (e) {
        console.log("e", e)
        res.status(500).send(new Error('Please try again later'));

    }
})


router.delete('/employeeDetails/:id', validationMiddleware(employeeSchema.deleteEmployeeSchema, "params"), async (req, res) => {
    try {
        const deletedEmployee = await employeeModel.findOneAndDelete({ "_id": req.params.id })
        if (deletedEmployee && Object.keys(deletedEmployee).length)
            res.send(deletedEmployee)
        else
        res.status(500).send(new Error('Please try again later'));

    }catch(e){
        res.status(500).send(new Error('Please try again later'));

    }
})