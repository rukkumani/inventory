const employee =require("../app/models/employeeModel.js")
const assert = require('assert');


describe('Creating employee', () => {
  it('creates a employee', (done) => {
     
      const savedEmployee = new employee({
        
        employeeRollNumber: '12345',
        employeeName:"rukkumani",
        employeeAge: 12,
        employeeMobileNumber:"1234543234",
        employeeEmail:"rukkumani@gmail.com",
        employeeAddress: [{
          street:"ggggggggggg",
          city: "ddddddddddd",
          state: "ddddddddd",
          zip: "hhhhhh"
        }
        ]});
        savedEmployee.save() 
          .then(() => {
              assert(!savedEmployee.isNew); //if poke is saved to db it is not new
              done();
          },()=>{
            done()
          }).catch(done())
  });
});


// describe('Employee', function () {
  //   describe('#save()', function () {
  //     it('should save without error', function (done) {
  //       var user = new employee({email:"rukku"});
  //       user.save(function (err,res) {
  //           console.log(err)
  //         if (err) done(err);
  //         else done();
  //       });
  //       done()
  //     });
  //   });
  // });