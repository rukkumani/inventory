const path = require('path'),
  rootPath = path.normalize(__dirname + '/..'),
  chalk = require('chalk'),
  env = process.env.NODE_ENV || 'development',
  dbHost = process.env.DB_HOST_NAME,
  dbPort = process.env.DB_PORT

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'Chair Inventory '
    },
    port:process.env.PORT|| 4001,
    db:"mongodb+srv://rukkumani:ruk@cluster0.l42vs.mongodb.net/employeeManagement?retryWrites=true&w=majority"
  }
};

module.exports = config[env];
logConfiguration();

function logConfiguration() {

  console.log(chalk.styles.green.open + chalk.styles.green.close);
  console.log("\n\n ---------------------Configuration in Use --------------------------")
  //    console.log(chalk.styles.blue.open);
  console.log(config[env])


}

