const express = require('express'),
    config = require('./config/config'),
    glob = require('glob'),
    mongoose = require('mongoose'),
    app = express();
    mongoose.Promise = global.Promise;


mongoose.connect(config.db,{useUnifiedTopology:true,useNewUrlParser:true});
var db = mongoose.connection;
db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
});


var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
    require(model);
});

require('./config/express')(app,config);


app.listen(config.port||3000,"0.0.0.0", function () {
    console.log('Express server listening on port ' + config.port);
});





